// let movieArray = ["test", "hi", "testing", "no test", "okay"]
let searchUserInput = document.getElementById("searchID");
let movieArray = [];
let movieToBeDeleted;
let htmlContent;
let movieListUlTag;
let checked = true;

searchUserInput.addEventListener("keyup", (event) => {
  // console.log(event.target.value);
  const searchValue = event.target.value;
  const filteredMovies = movieArray.filter((movie) => {
    return movie.includes(searchValue);
  });
  console.log(filteredMovies);

  displayMovies(filteredMovies);
  localStorage.setItem('movieArray',  JSON.stringify(filteredMovies));
});

function displayMovies(movieArray) {
  htmlContent = movieArray
    .map((movie) => {
      movieToBeDeleted = movie;
      return `
            <li class="movie-item" id="list">
                <span >${movie}
                <button id="delete" onClick="deleteMovie(event)" class="delete-button">Delete</button>
                </span>
            </li>
        `;
    })
    .join("");
    movieListUlTag.innerHTML = htmlContent;
   
}


function addMovie(e) {
  e.preventDefault();
  let userInput;
  userInput = document.getElementById("userID").value;
  if(userInput.length>=1){
    movieArray.push(userInput)
  }else{
    alert("Please enter a value")
  }
  movieListUlTag = document.getElementById("movie-list");
  // display movies
  displayMovies(movieArray);

}

function deleteMovie(event){
  console.log(movieToBeDeleted)
  let movieElement = event.target.parentNode;
  movieElement.remove();
  let movieIndex = movieArray.indexOf(movieToBeDeleted);
  movieArray.splice(movieIndex-1, 1)
  // removing movie-list classname 
  let div = document.getElementById("list");    
  div.classList.remove("movie-item");
}


function hideElements(event){
  // checked=true
  let element = document.querySelectorAll("li");
  if (checked){
    element.forEach(element => {
    element.style.display = 'none';
  });
  checked=false
console.log(checked, "checked")
}else{
  // element.forEach(element => {
  //   element.style.display = 'initial';
  // });
  displayMovies(movieArray)
  checked=true;
  console.log(checked, "unchecked")
}
  // console.log(element)
}


